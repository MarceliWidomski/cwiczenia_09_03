#include <iostream>
using namespace std;

int main() { //wypisuje liczby pierwsze z przedzialu z wykorzystaniem petli while
	cout << "Program wypisuje liczby pierwsze z zadanego przedzialu" << endl;
	int poczatek;
	int koniec;
	cout << "Podaj poczatek przedzialu: ";
	cin >> poczatek;
	cout << "Podaj koniec przedzialu: ";
	cin >> koniec;
	if (poczatek > koniec) { // sprawia ze zawsze liczba mniejsza jest poczatkiem przedzialu bez wprowadzania dodatkowej zmiennej
		poczatek = poczatek - koniec;
		koniec = koniec + poczatek;
		poczatek = koniec - poczatek;
	}
	int liczba(poczatek);
	int dzielnik(2);
	while (liczba <= koniec) {
		while (dzielnik <= liczba) {
			if (dzielnik == liczba) {
				cout << liczba << " ";
			}
			if (liczba % dzielnik == 0) { // przerywa petle gdy znajdzie dzielnik liczby
				break;
			}
			dzielnik++;
		}
		liczba++;
		dzielnik = 2; // po wykonaniu petli wewnetrznej, ustawia wartosc dzielnika na 2
	}
	return 0;
}
