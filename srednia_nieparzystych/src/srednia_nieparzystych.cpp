#include <iostream>
using namespace std;

int main() { // wybiera liczby nieparzyste z podanych i oblicza dla nich srednia
	int liczba(1);
	int suma(0);
	int a(0);
	float wynik;
	cout
			<< "Program wybiera liczby niaparzyste z podanych i oblicza ich srednia."
			<< endl;
	while (liczba != 0) {
		cout << "Podaj liczbe: ";
		cin >> liczba;
		if (liczba > 0 && (liczba & 1) == 1) { // zadany iloczyn bitowy zwraca 1 dla liczb niaparzystych
			a++;
			suma += liczba;
		}
	}
	if (a == 0) // zabezpiecza przed dzieleniem przez 0 w przypadku gdy podane zostana same liczby parzyste lub nie podana zostanie zadna liczba
		a = 1;
	wynik = 1.0*suma / a;
	cout << "Srednia podanych liczb to: " << wynik << endl;
	return 0;
}
