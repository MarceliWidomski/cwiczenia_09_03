#include <iostream>
using namespace std;

int main() { //sprawdza czy podana data istnieje
	int dzien(0);
	int miesiac(0);
	int rok(0);
	bool czyPrzestepny(0);
	bool czyIstnieje(0);
	int opcja;
	cout << "Program sprawdza czy podana data istnieje" << endl;
	while (opcja != 5) { // przerywa petle po wybraniu opcji wylacz
		cout << "Menu" << endl;
		cout << "Wybierz numer opcji: " << endl;
		cout << "1) Wprowadz rok" << endl;
		cout << "2) Wprowadz miesiac" << endl;
		cout << "3) Wprowadz dzien" << endl;
		cout << "4) Sprawdz czy podana data istnieje" << endl;
		cout << "5) Wylacz" << endl;
		cin >> opcja;
		switch (opcja) {
		case 1: {
			cout << endl;
			cout << "Podaj rok. Rok p. n. e. podaj ze znakiem \"-\"." << endl;
			cout << "Rok: ";
			cin >> rok;
			cout << endl;
			break;
		}
		case 2: {
			cout << endl;
			cout << "Miesiac: ";
			cin >> miesiac;
			cout << endl;
			break;
		}
		case 3: {
			cout << endl;
			cout << "Dzien: ";
			cin >> dzien;
			cout << endl;
			break;
		}
		case 4: {
			if ((rok % 4 == 0 && rok % 100 != 0) || rok % 400 == 0) // sprawdza czy zadany rok jest przestepny
				czyPrzestepny = 1;
			else
				czyPrzestepny = 0;
			if (dzien < 1 || dzien > 31 || miesiac < 1 || miesiac > 12)
				czyIstnieje = 0;
			else if (dzien == 31
					&& (miesiac == 4 || miesiac == 6 || miesiac == 9
							|| miesiac == 11))
				czyIstnieje = 0;
			else if (miesiac == 2
					&& (dzien > 29 || (czyPrzestepny == 0 && dzien > 28)))
				czyIstnieje = 0;
			else
				czyIstnieje = 1;
			cout << endl;

			if (czyIstnieje == 0) {
				cout << "Data " << dzien << " " << miesiac << " " << rok
						<< " nie istnieje." << endl;
			} else {
				cout << "Data " << dzien << " " << miesiac << " " << rok
						<< " istnieje." << endl;
			}
			cout << endl;
			break;
		}
		case 5: {
			cout << "Wylaczam...";
			break;
		}
		default: {
			cout << endl;
			cout << "Niewlasciwy wybor opcji. Sprobuj ponownie" << endl;
			cout << endl;
		}
		}
	}
	return 0;
}
