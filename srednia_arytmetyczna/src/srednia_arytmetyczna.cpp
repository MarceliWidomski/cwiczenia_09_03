#include <iostream>
using namespace std;

int main() {
	cout
			<< "Program oblicza srednia arytmetyczna podanych liczb. Wpisanie liczby 0 powoduje podanie wyniku."
			<< endl;
	double liczba(1);
	double suma(0);
	int a(-1);
	double wynik;
	while (liczba != 0) {
		cout << "Podaj liczbe: ";
		cin >> liczba;
		a++;
		suma += liczba;
	}
	wynik = suma / a;
	cout << "Srednia podanych liczb to: " << wynik << endl;
	return 0;
}
