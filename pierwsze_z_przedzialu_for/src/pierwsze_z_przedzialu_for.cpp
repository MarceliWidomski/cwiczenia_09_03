#include <iostream>
using namespace std;

int main() {//wypisuje liczby pierwsze z przedzialu z wykorzystaniem petli for
	cout << "Wypisuje liczby pierwsze z zadanego przedzialu" << endl;
	int poczatek;
	int koniec;
	cout << "Podaj poczatek przedzialu: ";
	cin >> poczatek;
	cout << "Podaj koniec przedzialu: ";
	cin >> koniec;
	if (poczatek > koniec) { // sprawia ze zawsze liczba mniejsza jest poczatkiem przedzialu bez wprowadzania dodatkowej zmiennej
		poczatek = poczatek - koniec;
		koniec = koniec + poczatek;
		poczatek = koniec - poczatek;
	}
	for (int i = poczatek; i <= koniec; ++i) {
		for (int j = 2; j <= i; ++j) {
			if (i == j)
				cout << i << " ";
			if (i % j == 0) // przerywa petle gdy znajdzie dzielnik liczby
				break;
		}
	}
	return 0;
}
