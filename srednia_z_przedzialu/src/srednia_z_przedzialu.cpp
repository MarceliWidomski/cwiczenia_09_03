#include <iostream>
using namespace std;

int main() { // oblicza srednia liczb z przedzialu <26.50>
	double liczba(1);
	double suma(0);
	int a(0);
	double wynik;
	cout
			<< "Program z podanych liczb wybiera te z przedzialu <26, 50> i oblicza ich srednia."
			<< endl;
	cout << "Wprowadzenie 0 powoduje podanie wyniku." << endl;
	while (liczba != 0) {
		cout << "Podaj liczbe: ";
		cin >> liczba;
		if (liczba >= 26 && liczba <= 50) {
			a++;
			suma += liczba;
		}
	}
	wynik = suma / a;
	cout << "Srednia podanych liczb to: " << wynik << endl;
	return 0;
}
