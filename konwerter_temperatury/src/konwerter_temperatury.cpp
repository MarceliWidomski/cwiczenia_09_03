#include <iostream>
using namespace std;

int main() {//przelicza temperature ze skali Celsjusza na Fahrenheita lub odwrotnie
	float tempF;
	float tempC;
	short int opcja;
	while (opcja != 3) {
		cout << "Menu" << endl;
		cout << "Wybierz numer opcji: " << endl;
		cout << "1) Przelicz temperature ze skali Fahrenheita na Celsjusza"
				<< endl;
		cout << "2) Przelicz temperature ze skali Celsjusza na Fahrenheita"
				<< endl;
		cout << "3) Wylacz" << endl;
		cin >> opcja;
		switch (opcja) {
		case 1: {
			cout << "Podaj temperature w F: ";
			cin >> tempF;
			tempC = (tempF - 32) * 5 / 9;
			cout << tempF << " F = " << tempC << " C" << endl;
			break;
		}
		case 2: {
			cout << "Podaj temperature w C: ";
			cin >> tempC;
			tempF = tempC * 9 / 5.0 + 32;
			cout << tempC << " C = " << tempF << " F" << endl;
			break;
		}
		case 3: {
			cout << "Wylaczam...";
			break;
		}
		default: {
			cout << "Niewlasciwa opcja. Sprobuj ponownie." << endl;
		}
		}
	}
	return 0;
}
